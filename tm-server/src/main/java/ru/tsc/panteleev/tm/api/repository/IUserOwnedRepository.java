package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

}
