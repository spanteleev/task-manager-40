package ru.tsc.panteleev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.panteleev.tm.model.Task;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Insert("INSERT INTO TM_TASK (row_id, name, description, status, created_dt, begin_dt, end_dt, user_id) "
            + "VALUES (#{id}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd}, #{userId})")
    void add(@NotNull Task task);

    @Update("UPDATE TM_TASK SET name = #{name}, description = #{description}, status = #{status}, " +
            "project_id = #{projectId} WHERE row_id = #{id}")
    void update(@NotNull Task task);

    @Insert("INSERT INTO TM_TASK (row_id, name, description, status, created_dt, begin_dt, end_dt, user_id, project_id) "
            + "VALUES (#{id}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd}, #{userId}, #{projectId})")
    void set(@NotNull Collection<Task> tasks);

    @NotNull
    @Select("SELECT * FROM TM_TASK WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Task> findAllByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Task> findAllSort(@NotNull SelectStatementProvider selectStatementProvider);

    @NotNull
    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Task> findAll();

    @Nullable
    @Select("SELECT * FROM TM_TASK WHERE user_id = #{userId} AND row_id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    Task findById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_TASK WHERE row_id = #{id}")
    void removeById(@NotNull String userId, @NotNull String id);

    @Delete("DELETE FROM TM_TASK WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE tm_task")
    void clear();

    @Select("SELECT COUNT(1) FROM TM_TASK WHERE user_id = #{userId}")
    long getSize(@Param("userId") @NotNull String userId);

    @Select("SELECT COUNT(1) = 1 FROM tm_task WHERE user_id = #{userId} AND row_id = #{id}")
    boolean existsById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @NotNull
    @Select("SELECT * FROM TM_TASK WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Task> findAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @Delete("DELETE FROM TM_TASK WHERE project_id = #{projectId}")
    void removeTasksByProjectId(@Param("projectId") @NotNull String projectId);

}
